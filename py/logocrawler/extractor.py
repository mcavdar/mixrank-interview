import traceback
import re
from threading import Thread, local
from queue import Queue
import download_wselenium

from bs4 import BeautifulSoup
import json

import logger
logging = logger.get_logger(__name__)


q_extract = Queue(maxsize=0)


def check_quality(result, response):
    if len(result) > 200:
        logging.info("result is too long for: "+response.url)
        return ""
    elif result[:2] == "//":
        return result[2:]
    elif result[0] == "/":
        return response.history[-1].url[:-1]+result if response.history else response.url[:-1]+result
    elif (re.search(response.history[-1].url.split("//")[1][:-1], result) if response.history else re.search(response.url.split("//")[1][:-1], result)):
        return result
    elif (re.search("http", result)):
        return result
    else:
        return response.url+result


def apply_rule(response, pattern, head):
    ''' Function to find first matche via regex.
    return empty string if there is no.
    '''
    for result in (re.finditer(pattern, head)):
        return check_quality(result.group(1), response)


def parse_html(response, initial_url, soup):
    ''' Helper function to define patterns.
    return 'Search unsuccessful' if no rule can be applied.
    '''
    head = str(soup.find("head"))
    body = str(soup.find("body"))

    patterns = ['<[^>]*[Ll]ogo[^>]{0,100}src=[\"\'](.*?)[\"\']',
                'src=[\"\']([^\"\']*)[\"\'][^>]*[Ll]ogo',
                'src=[\"\']([^\"\']*'+(response.history[-1].url.split("//")[1]
                                       if response.history else response.url.split("//")[1]) + '[^\"\']*[Ll]ogo[^\"\']*)[\"\']',
                'src=[\"\']([^\"\']*[Ll]ogo[^\"\']*)[\"\']']
    match_flag = False
    for rule_id, pattern in enumerate(patterns):
        if(re.search(pattern, head)):
            logging.info("@head - rule " + str(rule_id+1) + ": " +
                         " for: " + initial_url)
            match_flag = True
            return apply_rule(response, pattern, head)
    for rule_id, pattern in enumerate(patterns):
        if(re.search(pattern, body)):
            logging.info("@body - rule " + str(rule_id+1) + ": " +
                         " for: " + initial_url)
            match_flag = True
            return apply_rule(response, pattern, body)
    if(not match_flag):
        #logging.info("Search unsuccessful for: "+ response.url)
        if('selenium' not in response.headers):
            download_wselenium.q_download_sel.put(initial_url)
            return "-1"
        logging.debug("html source for {}: {}".format(
            initial_url, response.text))
        return "Nan"


def check_ldjson_and_og(response, soup):
    ''' Function to check whether html source contains og:logo, og:image meta or [Ll]ogo field in ld+json data.
    '''
    logo = soup.find("meta", property="og:logo")
    image = soup.find("meta", property="og:image")
    if logo:
        logging.info("og:logo found: "+response.url)
        return check_quality(logo["content"], response)
    elif image:
        try:
            if(re.search('[Ll]ogo', image["content"])):
                logging.info("og:image+logo found: "+response.url)
                return check_quality(image["content"], response)
        except Exception as e:
            logging.info("og:image content error: "+str(image))
    else:
        json_tag = soup.find("script", {"type": "application/ld+json"})
        if(json_tag):
            parsed_json = json.loads("".join(json_tag))
            if 'logo' in parsed_json:
                logging.info("logo found in ld+json: "+response.url)
                if(type(parsed_json['logo']) == dict):
                    return check_quality(parsed_json['logo']['url'], response)
                else:
                    return check_quality(parsed_json['logo'], response)
            elif 'Logo' in parsed_json:
                logging.info("Logo found in ld+json: "+response.url)
                if(type(parsed_json['Logo']) == dict):
                    return check_quality(parsed_json['Logo']['url'], response)
                else:
                    return check_quality(parsed_json['Logo'], response)
    return ""


def check_meta_img_itemprop(response, soup):
    '''
    patterns=[("meta", {"itemprop":"image"}),
    ("meta", {"itemprop":"logo"}),
    ("img", {"itemprop":"image"}),
    ("img", {"itemprop":"logo"}),
    ("meta", {"content":"image"}),
    ("meta", {"content":"logo"})]
    '''

    patterns = [("meta", {"itemprop": "logo"}),
                ("img", {"itemprop": "logo"}),
                ("meta", {"content": "logo"})]

    for pattern in patterns:
        result = soup.find(pattern[0], pattern[1])
        if result:
            logging.info("check_meta:"+pattern[0]+str(pattern[1])+str(result))
            if result['src']:
                return check_quality(result['src'], response)
            elif result['data-src']:
                return check_quality(result['data-src'], response)
            elif result['content']:
                return check_quality(result['content'], response)
    else:
        return ""
    return ""

    #meta_name_msapplication=str(soup.find("meta", name="msapplication-TileImage"))
    # logging.info("meta_name_msapplication"+meta_name_msapplication)

    #img_alt_logo=str(soup.find("img", alt="logo"))
    #img_src_logo=str(soup.find("img", src="image"))

    # logging.info("img_alt_logo"+img_alt_logo)
    # logging.info("img_src_logo",img_src_logo)


def find_svg(response, soup):
    ''' Function to test whether html source contain embedded svg as a logo.
    TBC
    '''
    result = soup.find("svg")
    if(re.search('[Ll]ogo', str(result))):
        logging.info("Svg tag found in soup: "+response.url[:-1])
        # logging.info(str(result))
        return 1
    return 0


def extract_logo():
    ''' Extract logo worker, get URL from queue until no url left in the queue
    and run other functions.
    '''
    while True:
        try:
            flag = True
            response, url = q_extract.get(timeout=30)
            extracted = url+","
            if(response):
                try:
                    soup = BeautifulSoup(response.text, "html.parser")
                    if((oglogo_ldjson := check_ldjson_and_og(response, soup))):
                        extracted += oglogo_ldjson
                    elif((meta_img_itemprop := check_meta_img_itemprop(response, soup))):
                        extracted += meta_img_itemprop
                    else:
                        if(not find_svg(response, soup)):
                            extracted_part = parse_html(response, url, soup)
                            if(extracted_part != "-1"):
                                if(extracted_part == "Nan"):
                                    logging.info(
                                        "couldn't find any result for: "+url)
                                extracted += extracted_part
                            else:
                                flag = False
                except Exception as e:
                    logging.info("Parsing issue for: "+url+"\n")
                    logging.info(e)
                    logging.info(traceback.format_exc())
            else:
                logging.info("Response is None for: "+url)
            if flag:
                print(extracted[7:])
        # if(do_request(url+"/favicon.ico")):
        #  print(extracted[7:]+", "+url+"/favicon.ico")
        # else:
        #  print(extracted[7:]+", ")
            q_extract.task_done()
        except Exception as e:
            logging.info("extract thread finishing since queue is empty")
            logging.info(e)
            return


def create_extract_threads():
    '''Start threads, each thread as a wrapper of html extractor'''
    thread_num = 10
    for i in range(thread_num):
        t_worker = Thread(target=extract_logo)
        t_worker.start()
    q_extract.join()
