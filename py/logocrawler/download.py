from requests.models import Response
from threading import Thread, local
import extractor
import download_wselenium

from requests.sessions import Session
from queue import Queue
from collections import OrderedDict
import re
import requests
import sys
import os

import logger
logging = logger.get_logger(__name__)


# Queue for download html source
q_download = Queue(maxsize=0)
thread_local = local()


def read_stdin():
    ''' function to read STDIN'''
    logging.info(f"starting read_stdin function")
    for line in sys.stdin:
        q_download.put("http://"+line.rstrip())
    logging.info(f"finishing read_stdin function")


def get_session():
    if not hasattr(thread_local, 'session'):
        headers = OrderedDict({
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Accept-Encoding': 'gzip, deflate, br',
            'DNT': '1',
            'Upgrade-Insecure-Requests': '1',
            'Connection': 'keep-alive',
            'referer': 'https://google.com/'
        })
        thread_local.session = requests.Session()  # Create a new Session if not exists
        thread_local.session.headers = headers
    return thread_local.session


def do_request(url):
    '''function to do request via session'''
    session = get_session()
    try:
        with session.get(url, timeout=3) as response:
            if response.status_code == 200:
                return response
            else:
                logging.info("response.status_code: " +
                             str(response.status_code) + "for:" + url)
                logging.info("let's try with Selenium")
                download_wselenium.q_download_sel.put(url)
                return -1
    except Exception as e:
        logging.info("requests.get() issue for: "+url)
        logging.info("let's try with Selenium")
        download_wselenium.q_download_sel.put(url)
        return -1


def download_html():
    '''download link worker, get URL from queue until no url left in the queue'''
    while True:
        try:
            url = q_download.get(timeout=30)
            response = do_request(url)
            if(response != -1):
                extractor.q_extract.put((response, url))
            q_download.task_done()          # tell the queue, this page is done
        except Exception as e:
            logging.info("download thread killing itself since queue is empty")
            return


def create_download_threads():
    '''Start threads, each thread as a wrapper of html downloader'''
    thread_num = 20
    for i in range(thread_num):
        t_worker = Thread(target=download_html)
        t_worker.start()
    q_download.join()                # main thread waits all others
