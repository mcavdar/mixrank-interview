import extractor
import download
import download_wselenium
import logger


def main():
    ''' logocrawler script reads URL from STDIN and returns relevant logo to STDOUT.

    You can test it with websites.csv file or just by giving input from stdin:
    1 ) python crawler.py < ../../websites.csv > output_file
    or if you would like to test a case just run:
    2 ) python crawler.py

    Please give URL without protocol prefix. (e.g. twitter.com)
    '''
    logging.info(f"starting main function")
    # threads to request html sources
    download.create_download_threads()
    download_wselenium.create_download_threads()
    logging.info(f"download threads have been created")
    # threads to extract logo(s) from html source(s)
    extractor.create_extract_threads()
    logging.info(f"extract threads have been created")
    # read STDIN
    download.read_stdin()
    logging.info(f"reading stdin completed")
    logging.info(f"finishing main function")


if __name__ == "__main__":
    logging = logger.initial_logger()
    main()
