import logging
import time


APP_LOGGER_NAME = 'logocrawler'


def initial_logger():
    logging.basicConfig(filename=str(time.time())+"_"+APP_LOGGER_NAME,
                        filemode='w',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%H:%M:%S',
                        level=logging.INFO)
    return logging.getLogger(APP_LOGGER_NAME)


def get_logger(module_name):
    return logging.getLogger(APP_LOGGER_NAME).getChild(module_name)
