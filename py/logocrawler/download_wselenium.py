import traceback
from requests.models import Response
from selenium.webdriver.firefox.options import Options
from selenium import webdriver
import selenium
from threading import Thread
import extractor

from queue import Queue
from collections import OrderedDict
import re
import requests
import sys
import os

import logger
logging = logger.get_logger(__name__)


# Queue for download html source
q_download_sel = Queue(maxsize=0)

'''
opts = Options()
opts.add_argument("--headless")
opts.add_argument("User-Agent=Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0") 
driver = webdriver.Firefox(options=opts)
'''


def do_request_wsel(url):
    '''function to do request via session'''
    opts = Options()
    opts.add_argument("--headless")
    opts.add_argument(
        "User-Agent=Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0")
    driver = webdriver.Firefox(options=opts)
    try:
        driver.set_page_load_timeout(10)
        driver.get(url)
        response = Response()
        response.headers.update({"selenium": "True"})
        response.url = url
        response._content = str.encode(driver.page_source)
        response.status_code = 200
        hist = Response()
        hist.url = url
        response.history.append(hist)
        driver.close()
        return response
    except Exception as e:
        logging.info("Selenium Error for:"+url)
        logging.info(e)
        logging.info(traceback.format_exc())
        try:
            driver.close()
        except Exception as e:
            logging.info("can't close driver:"+url)
            logging.info(e)
            logging.info(traceback.format_exc())


def download_html_wsel():
    '''download link worker, get URL from queue until no url left in the queue'''
    while True:
        try:
            url = q_download_sel.get(timeout=30)
            response = do_request_wsel(url)
            extractor.q_extract.put((response, url))
            q_download_sel.task_done()          # tell the queue, this page is done
        except Exception as e:
            logging.info("download thread killing itself since queue is empty")
            logging.info(e)
            logging.info(traceback.format_exc())
            return


def create_download_threads():
    '''Start threads, each thread as a wrapper of html downloader'''
    thread_num = 50
    for i in range(thread_num):
        t_worker = Thread(target=download_html_wsel)
        t_worker.start()
    q_download_sel.join()                # main thread waits all others
