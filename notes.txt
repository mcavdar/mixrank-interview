How to use?
============================
1 ) python crawler.py < ../../websites.csv > output_file
or if you would like to test a case run:
2 ) python crawler.py


Let's see some statistics:
============================
We log script output in a file(with timestamp prefix) thus we can grep with "grep pattern 16*".

-----------------------------------------------
1 - "$ grep "Selenium Error for:.*" 16* | wc -l"
177
# We can't/don't crawl 177 domains, even with Selenium.

-----------------------------------------------
2- "$ grep "Svg tag found in soup" 16* | wc -l"
60
# 60 domains contain embedded SVG logo thus we can't refer their logo with a URL.

-----------------------------------------------
3- "$ grep "couldn't find any result" 16*  | wc -l"
139
# For 139 domains script couldn't find a logo.

-----------------------------------------------
4- "$ grep "[Ll]ogo found in ld+json" 16*   | wc -l"
22
# Script found linked data json with logo for 22 domains.

-----------------------------------------------
5- "$ grep "og:.*logo" 16*   | wc -l"
100
# Script found 'og:logo' or 'og:image' tags for 100 domains.
# for details https://ogp.me/

-----------------------------------------------
6- "$ grep "check_meta" 16* | wc -l"
11
# Script found 'meta/img itemprop="logo"' tags for 11 domains.

-----------------------------------------------
7- "$ grep "@[a-z]* - rule[^:]*:" 16*  -o | sort | uniq -c "
    359 @body - rule 1:
     16 @body - rule 2:
     35 @body - rule 3:
     75 @body - rule 4:
      4 @head - rule 1:
      1 @head - rule 2:
      1 @head - rule 4:
# Some details about heuristic rules.

-----------------------------------------------
8- "$ cut -d, -f2 output_file | sort | uniq -c  | sort -n"
...
      2 https://www.lg.com/lg5-common-gp/images/common/header/logo-b2c-m.jpg
      2 https://www.poly.com/etc.clientlibs/poly/clientlibs/clientlib-site/resources/images/company-logo-color.svg
      2 https://www.register.com/content/dam/register/target/mktg-13867/web-logo.png
      2 https://www.spiceworks.com/data:image/svg+xml
      2 https://www.yahoo.com/{promoLogo}
      3 assets.yolacdn.net/logos/yola-white-ua-9de6e7ea.png
      3 https://cdn.comcast.com/-/media/E020F7373720426891AC068F01DA5A2A
    139 Nan
    266 
# We couldn't find logo for ~400 domains. 266 of them contains SVG embedded logo or gives "Selenium Error"

